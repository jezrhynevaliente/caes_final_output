-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 18, 2020 at 04:45 AM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.3.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `caes`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin_account`
--

CREATE TABLE `admin_account` (
  `admin_id` int(11) NOT NULL,
  `fullname` varchar(65) NOT NULL,
  `username` varchar(65) NOT NULL,
  `email` varchar(65) NOT NULL,
  `password` varchar(65) NOT NULL,
  `secret_password` varchar(65) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin_account`
--

INSERT INTO `admin_account` (`admin_id`, `fullname`, `username`, `email`, `password`, `secret_password`) VALUES
(1, 'Jezarene Valiente', 'jvaliente', 'jezarenevaliente@gmail.com', 'jezarene0130', 'weareone1485');

-- --------------------------------------------------------

--
-- Table structure for table `exam`
--

CREATE TABLE `exam` (
  `exam_id` int(11) NOT NULL,
  `type` int(65) NOT NULL,
  `question` text NOT NULL,
  `answer` text NOT NULL,
  `choice_one` text NOT NULL,
  `choice_two` text NOT NULL,
  `choice_three` text NOT NULL,
  `quiz_status` int(11) NOT NULL,
  `tama` int(11) NOT NULL,
  `mali` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `exam`
--

INSERT INTO `exam` (`exam_id`, `type`, `question`, `answer`, `choice_one`, `choice_two`, `choice_three`, `quiz_status`, `tama`, `mali`) VALUES
(8, 5, 'Seoul is the capital of South Korea.', 'True', 'False', 'N/A', 'N/A', 0, 0, 0),
(10, 1, 'The Korean boy group known as the King of Kpop and The Nation\'s Pick.', 'EXO', 'GOT7', 'NCT DREAM', 'ASTRO', 0, 0, 0),
(11, 2, 'What color represent Philippines?', 'Yellow, white, red and blue', 'Yellow, green, blue and violet', 'White, blue, red and green', 'Red, white, yellow and green', 0, 0, 0),
(13, 2, 'What country is sushi from?', 'Japan', 'Thailand', 'China', 'Korean', 0, 0, 0),
(14, 3, 'According to Old Testament, how many days did it take God to create the world?', '6', '7', '5', '4', 0, 0, 0),
(15, 4, 'Which is the world\'s most populous country?', 'China', 'India', 'America', 'Brazil', 0, 0, 0),
(16, 3, 'About what percentage of the earth\'s surface is water?', '70%', '10%', '50%', '95%', 0, 0, 0),
(17, 5, 'Washington, D.C. is a state.', 'False', 'True', 'N/A', 'N/A', 0, 0, 0),
(18, 1, 'When should you take antibiotics?', 'For bacterial infections', 'For viruses', 'Never', 'Whenever you feel sick', 0, 0, 0),
(19, 5, 'The shape of a pregnant belly can help parents predict the sex of their baby.', 'False', 'True', '', '', 0, 0, 0),
(20, 5, 'Africa is a country.', 'False', 'True', 'N/A', 'N/A', 0, 0, 0),
(21, 4, 'What is the approximate number of islands that comprise the Philippines?', '7500', '6500', '8500', '9500', 0, 1, 0),
(22, 1, 'When did the Philippines become independent?', 'July 04, 1946', 'July 04, 1976', 'August 14, 1947', 'Jan 01, 1984', 0, 0, 0),
(23, 2, 'Which country occupied the Philippines during World War II?', 'Japan', 'Germany', 'America', 'China', 0, 0, 0),
(24, 3, 'What is the term of the President of the Philippines?', '6(six) years', '3(three) years', '4(four) years', '10(ten) years', 0, 0, 0),
(25, 4, 'Which of the following groups has the Philippines as its member?', 'ASEAN', 'NATO', 'SAARC', 'OAU', 0, 0, 0),
(26, 1, 'Which is the predominant religion of the Philippines?', 'Christianity', 'Islam', 'Buddhism', 'Judaism', 0, 0, 0),
(27, 2, 'Which country had the Philippines as its colony for more than 300 years?', 'Spain', 'Italy', 'France', 'England', 0, 0, 0),
(28, 3, 'What is the currency used in Japan?', 'Yen', 'Yuan', 'Won', 'Euro', 0, 0, 0),
(29, 4, 'What currency used in Korea?', 'Won', 'Yen', 'Yuan', 'Peso', 0, 0, 0),
(30, 1, 'Which color must be mixed together to make green?', 'Blue and yellow', 'Red and yellow', 'Blue and white', 'Yellow and white', 0, 0, 0),
(31, 2, 'When Michael Jordan played for the Chicago Bulls, how many NBA Championships did he win?', 'Six', 'Seven', 'Four', 'Five', 0, 0, 0),
(32, 3, 'What year was the very first model of the iPhone released?', '2007', '2005', '2010', '2019', 0, 0, 0),
(33, 1, 'What\'s the shortcut for the \"copy\" function on most computers?', 'Ctrl C', 'Ctrl V', 'Ctrl X', 'Ctrl Z', 0, 0, 0),
(34, 3, 'What does \"HTTP\" stands for?', 'Hypertext Transfer Protocol', 'Hypertext Transmission Program', 'Hyperlink Transission Protocol', 'Hypertext Transfer Program', 0, 0, 0),
(35, 2, 'What is often seen as the smallest unit of memory?', 'Kilobyte', 'Gigabyte', 'Terabyte', 'Kilogram', 0, 0, 0),
(36, 5, 'Hotmail is an email service owned by Microsoft.', 'True', 'False', 'N/A', 'N/A', 0, 0, 0),
(37, 4, 'Who is often called the father of the computer?', 'Charles Babbage', 'Ada Lovelace', 'Carl Jung', 'Henry Sy', 0, 0, 0),
(38, 5, 'Java is a type of OS.', 'False', 'True', 'N/A', 'N/A', 0, 0, 0),
(39, 1, 'What was Twitter\'s original name?', 'twttr', 'i-tweet', 'twitty', 'tweet tweet', 0, 0, 0),
(40, 2, 'Which planet is the hottest in the solar system?', 'Venus', 'Mars', 'Jupiter', 'Mercury', 0, 0, 0),
(41, 3, 'What is the symbol for potassium?', 'K', 'H', 'Cl', 'P', 0, 0, 0),
(42, 4, 'Which planet has the most gravity?', 'Jupiter', 'Earth', 'Uranus', 'Mercury', 0, 0, 0),
(43, 1, 'Which country produces the most coffee in the world?', 'Brazil', 'South Korea', 'China', 'Japan', 0, 0, 0),
(44, 2, 'Which country invented tea?', 'China', 'Japan', 'Singapore', 'Thailand', 0, 0, 0),
(45, 3, 'Which European nation was said to invent hot dogs?', 'Germany', 'United States', 'Russia', 'France', 0, 0, 0),
(46, 4, 'Which country is responsible for giving us pizza and pasta?', 'Italy', 'Taiwan', 'Indonesia', 'India', 0, 0, 0),
(47, 1, 'Which organ has four chambers?', 'The Heart', 'The Brain', 'The Lungs', 'The Liver', 0, 0, 0),
(48, 2, 'What percentage of our bodies is made up of water?', '60-65%', '55-60%', '80-95%', '65-75%', 0, 0, 0),
(49, 3, 'Which element is said to keep bones strong?', 'Calcium', 'Iron', 'Potassium', 'Hydrogen', 0, 0, 0),
(50, 4, 'How many times does the heart beat per day?', 'More than 100 000', 'Less than 100 000', 'Exactly 100 000', 'More than 1 000 000', 0, 0, 0),
(51, 1, 'What is your body\'s largest organ?', 'Skin', 'Heart', 'Lungs', 'Brain', 0, 0, 0),
(52, 2, 'Which continent is the largest?', 'Asia', 'Europe', 'Antarctica', 'Africa', 0, 0, 0),
(53, 3, 'Which of the Seven Wonders is located in Egypt?', 'The Pyramid of Giza', 'Great wall of China', 'Temple of Artemis', 'Lighthouse of Alexandria', 0, 0, 0),
(54, 4, 'What desert is the largest in the world?', 'The Sahara Desert', 'The Namib Desert', 'The Gobi Desert', 'The White Desert, Farafra', 0, 0, 0),
(55, 1, 'What is the name of the world\'s longest river?', 'Nile River', 'Mississippi River', 'Amazon River', 'Danube River', 0, 0, 0),
(56, 2, 'How many Grammys does John Legend have?', 'Ten', 'Eight', 'Seven', 'Five', 0, 0, 0),
(57, 3, 'What is the title of the song that Queen Elsa sings as she built her ice castle in the movie Frozen?', 'Let it Go', 'Boombayah', 'Love Shot', 'Despacito', 0, 0, 0),
(58, 4, 'Which popular TV show features house Targaryen and Stark?', 'Game of Thrones', 'Star Trek', 'Star Wars', 'Harry Potter', 0, 0, 0),
(59, 1, 'How many Lord of the Rings films are there?', '3', '5', '9', '7', 0, 0, 0),
(60, 2, 'Which cartoon character lives in a pineapple under the sea?', 'Patrick', 'Spongebob', 'Doraemon', 'Dora', 0, 0, 0),
(61, 3, 'How many hearts does an octopus have?', 'Three', 'Eight', 'Five', 'Four', 0, 0, 0),
(62, 4, 'How many eyes does a bee have?', 'Five', 'Seven', 'Three', 'Eight', 0, 0, 0),
(63, 1, 'What is a female donkey called?', 'AJenny', 'ARose', 'AJisoo', 'ALisa', 0, 0, 0),
(64, 2, 'Which mammals has no vocal chords?', 'Giraffe', 'Zebra', 'Elephant', 'Rabbit', 0, 0, 0),
(65, 3, 'What is the fastest animal in th world?', 'Cheetah', 'Lion', 'Tiger', 'Kangaroo', 0, 0, 0),
(66, 4, 'Which animal symbolizes good luck in Europe?', 'Ladybug', 'Grasshopper', 'Ant', 'Dragon Fly', 0, 0, 0),
(67, 5, 'Sponges have hearts.', 'False', 'True', 'N/A', 'N/A', 0, 0, 0),
(68, 1, 'What is the capital of Thailand?', 'Bangkok', 'Pyongchang', 'Manila', 'Seoul', 0, 0, 0),
(69, 2, 'Which city is known as the \"Walled City?\"', 'Malolos', 'Intramurous', 'Makati', 'Baguio', 0, 0, 0),
(70, 3, 'The Bataan Death March took place in what year?', '1942', '1869', '1980', '1890', 0, 0, 0),
(71, 4, 'Who was known as the \"Hero of Tirad Pass?\"', 'Gregorio Del Pilar', 'Andres Bonifacio', 'Jose Rizal', 'Emilio Aguinaldo', 0, 0, 0),
(72, 1, 'In what year did the Portuguese explorer Ferdinand Magellan arrive in the shores of Philippines?', '1521', '1552', '1621', '1721', 0, 0, 0),
(73, 2, 'Mayon Volcano is located in which province?', 'Albay', 'Cagayan', 'Pangasinan', 'Tagaytay', 0, 0, 0),
(74, 3, 'Who was the first Filipina to win the Miss International beauty title in 1964?', 'Gemma Cruz', 'Gloria Diaz', 'Margarita Moran', 'Megan Young', 0, 0, 0),
(75, 4, 'Gloria Diaz won the Miss Universe contest in what year?', '1969', '1890', '1954', '1965', 0, 0, 0),
(76, 1, 'What pen name did Marcelo H del Pilar use in his writings?', 'Plaridel', 'Mongol', 'Parker', 'Ballpen', 0, 0, 0),
(77, 2, 'Which Filipino boxer is known for his nickname \"Pac-Man?\"', 'Manny Pacquiao', 'Efren \"Bata\" Reyes', 'Penalosa', 'Benji Paraz', 0, 0, 0),
(78, 3, 'Who said this immortal words \"A Filipino is worth dying for?\" ', 'Ninoy Aquino', 'Diosdado Macapagal', 'Emilio Jacinto', 'Ferdinand Marcos', 0, 0, 0),
(79, 4, 'What is a fertilized duck egg called?', 'Balut', 'Tokneneng', 'Itlog', 'Pugo', 0, 0, 0),
(80, 1, 'What is the meaning of the acronym NAMFREL?', 'National Citizen\'s Movement for free Elections', 'National Membership Card', 'National Movement Free Election', 'None of the above', 0, 0, 0),
(81, 2, 'What is the original name of Luneta park?', 'Bagumbayan', 'LunetaPark', 'Pook Pasyalan', 'Pasyalan', 0, 0, 0),
(82, 3, 'What are the provinces that consist of the acronym CALABARZON? (Name them)', 'Cavite, Laguna, Batangas, Rizal, Quezon', 'Cagayan, Laguna, Bataan, Rizal, Quirino', 'Cagayan, Laguna, Batanes, Romblon,Quirino', 'Cavite, Laguna, Bataan, Rizal, Quirino', 0, 0, 0),
(83, 4, 'Mt Pinatubo is located in the province of Luzon at the intersection of the borders of the provinces of Zambales, Tarlac, and ______.', 'Pampanga', 'Batanes', 'Iloilo', 'Pangasinan', 0, 0, 0),
(84, 4, 'Where in Manila can you find the San Agustin Church?', 'Intramuros', 'Manila', 'Makati', 'Batangas', 0, 0, 0),
(85, 1, '\"Pahiyas\" is a festival celebrated every May in which town in Quezon province?', 'Luban', 'Laoag', 'Palawan', 'Pangasinan', 0, 0, 0),
(86, 2, 'What is the residence of the President of the Philippines popularly called?', 'MalacaÃ±ang Palace', 'Presidential Palace', 'White House', 'Blue House', 0, 0, 0),
(89, 3, 'This computer abbreviation usually means ?', 'Escape', 'Space', 'Backspace', 'Enter', 0, 0, 0),
(90, 4, 'Which computer symbol matches this name?\r\nColon', ':', '&', ' _', ' ;', 0, 0, 0),
(91, 1, 'What is a common name for this computer symbol?\r\n( )', 'Parenthesis', 'Quotation', 'Curly Bracket', 'Bracket', 0, 0, 0),
(92, 2, 'Which computer symbol matches this name?\r\nAsterisk', '*', ';', '#', '&', 0, 0, 0),
(93, 3, 'This extension refers usually to what kind of file?\r\n.COM', 'System file', 'Image file', 'Document file', 'Powerpoint file', 0, 0, 0),
(94, 4, 'What is a common name for this computer symbol?\r\nÂ£', 'Sterling Dollar ', 'Euro', 'Pound', 'Yen', 0, 0, 0),
(95, 1, 'Which computer symbol matches this name?\r\nPeriod/Full stop', '.', '*', ',', '#', 0, 0, 0),
(96, 2, 'This extension refers usually to what kind of file?\r\n.PSD', 'Image file', 'Adobe Acrobat file', 'MS Office document ', 'Compressed Archive file', 0, 0, 0),
(97, 3, 'A house mouse and also a friend of the titular cats (The Aristocats)\r\n', 'Roquefort', 'Kristoff', 'Dormouse', 'Sebastian', 0, 0, 0),
(98, 4, 'The strong and aggressive Native-American warrior who was asked to marry the titular character (Pocahontas)\r\n', 'Kocoum', 'Tantor', 'Rafiki', 'Wasabi', 0, 0, 0),
(99, 1, 'The wise and elderly leader of the Trolls (Frozen)', 'Grand Pabbie', 'Chip', 'Jose Carioca', 'Jumba Jookiba', 0, 0, 0),
(100, 2, 'Celestial chariot pulled by two lions', 'Rhea', 'Hecate', 'Themis', 'Selene', 0, 0, 0),
(101, 3, 'Flute and goat', 'Pan', 'Hermes', 'Zues', 'Hades', 0, 0, 0),
(102, 4, 'God of War\r\n', 'Mars', 'Mercury', 'Jupiter', 'Apollo', 0, 0, 0),
(103, 1, 'Febris\r\n', 'Goddess of protection against fevers', 'Goddess of healing', 'Goddess of seawater, wife of Neptune', 'Goddess of bees and beekeeping', 0, 0, 0),
(104, 2, 'Loyal wife of Odysseus\r\n', 'Penelope', 'Atalanta', 'Hippolyte', 'Helen', 0, 0, 0),
(105, 3, 'Beautiful wife of king Cepheus, mother of Andromeda.', 'Cassiopeia', 'Penelope', 'Callisto', 'Ariadne', 0, 0, 0),
(106, 4, 'Princess loved by Eros', ' Psyche ', ' Arachne', ' Antiope', 'Io', 0, 0, 0),
(107, 5, 'The capital of Libya is Benghazi.', 'False', 'True', 'N/A', 'N/A', 0, 0, 0),
(108, 5, 'Albert Einstein was awarded the Nobel Prize in Physics.', 'True', 'False', 'N/A', 'N/A', 0, 0, 0),
(109, 5, 'Baby koalas are called joeys.', 'True', 'False', 'N/A', 'N/A', 0, 0, 0),
(110, 5, 'Gone with the Wind takes place in Savannah, Georgia.', 'False', 'True', 'N/A', 'N/A', 0, 0, 0),
(111, 5, 'Brazil is the only country in the Americas whose official language is Portuguese.', 'True', 'False', 'N/A', 'N/A', 0, 0, 0),
(112, 5, 'The American Civil War ended in 1776.', 'False', 'True', 'N/A', 'N/A', 0, 0, 0),
(113, 5, 'A right triangle can never be equilateral.', 'True', 'False', 'N/A', 'N/A', 0, 0, 0),
(114, 5, 'â€œPaprikaâ€ is the Hungarian word for â€œpepper.â€', 'True', 'False', 'N/A', 'N/A', 0, 0, 0),
(115, 5, 'The Bill of Rights contains 10 amendments to the Constitution.', 'True', 'False', 'N/A', 'N/A', 0, 0, 0),
(116, 5, 'Snow White\'s seven dwarfs all worked as lumberjacks.', 'False', 'True', 'N/A', 'N/A', 0, 0, 0),
(117, 5, 'The favorite food of the Teenage Mutant Ninja Turtles is pizza.', 'True', 'False', 'N/A', 'N/A', 0, 0, 0),
(118, 5, 'No bird can fly backwards.', 'False', 'True', 'N/A', 'N/A', 0, 0, 0),
(119, 5, 'There are seven red stripes in the United States flag.', 'True', 'False', 'N/A', 'N/A', 0, 0, 0),
(120, 5, 'The first name of Kramer in Seinfeld is Cosmo.', 'True', 'False', 'N/A', 'N/A', 0, 0, 0),
(121, 1, 'What is the color of the sky?', 'Sky Blue', 'Green', 'Red', 'Yellow', 0, 0, 0),
(122, 5, 'Great wall of China is one of the Seven Wonders of the World.', 'True', 'False', 'N/A', 'N/A', 0, 0, 0),
(123, 1, 'Where I studied in High School?', 'Lyceum of Alca', 'St. Philomene Academy', 'St. Paul University Philippines', 'Cagayan National High School', 0, 0, 0),
(124, 5, 'The model of my phone is Oppo A37.', 'True', 'False', 'N/A', 'N/A', 0, 0, 0),
(125, 1, 'When is the birthday of Jesus Christ?', 'December 25', 'January 1', 'September 18', 'July 12', 0, 0, 0),
(126, 5, 'Mama Mary is the mother of Jesus.', 'True', 'False', 'N/A', 'N/A', 0, 0, 0),
(127, 1, 'When is the birthday of Jesus Christ?', 'December 25', 'September 08', 'January 1', 'July 12', 0, 0, 0),
(128, 5, 'Mama Mary is the mother of Jesus.', 'True', 'False', 'N/A', 'N/A', 0, 0, 0),
(129, 1, 'When is the birthday of Jesus?', 'December 25', 'January 1', 'September 8', 'July 12', 0, 0, 0),
(130, 5, 'Mama Mary is the mother of Jesus.', 'True', 'False', 'N/A', 'N/A', 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `login_status`
--

CREATE TABLE `login_status` (
  `id` int(11) NOT NULL,
  `username` varchar(65) NOT NULL,
  `date` date NOT NULL,
  `hour` varchar(65) NOT NULL,
  `day` varchar(65) NOT NULL,
  `score` int(11) NOT NULL,
  `items` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `login_status`
--

INSERT INTO `login_status` (`id`, `username`, `date`, `hour`, `day`, `score`, `items`) VALUES
(3, 'jeza', '2020-03-16', '06:56:21pm', 'Monday', 3, 5),
(5, 'jeza', '2020-03-16', '07:00:50pm', 'Monday', 1, 5),
(6, 'jeza', '2020-03-16', '07:26:36pm', 'Monday', 2, 5),
(7, 'jeza', '2020-03-16', '07:28:10pm', 'Monday', 3, 5),
(8, 'jeza', '2020-03-16', '07:29:55pm', 'Monday', 4, 5),
(47, 'jvaliente', '2020-03-17', '10:59:18pm', 'Tuesday', 3, 5),
(49, 'jezarene', '2020-03-17', '11:04:21pm', 'Tuesday', 3, 5);

-- --------------------------------------------------------

--
-- Table structure for table `user_accounts`
--

CREATE TABLE `user_accounts` (
  `user_id` int(11) NOT NULL,
  `fullname` varchar(65) NOT NULL,
  `username` varchar(65) NOT NULL,
  `email` varchar(65) NOT NULL,
  `password` varchar(65) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_accounts`
--

INSERT INTO `user_accounts` (`user_id`, `fullname`, `username`, `email`, `password`) VALUES
(1, 'Jezarene Valiente', 'jeza', 'jezarenevaliente@gmail.com', 'jeza'),
(2, 'Wardita Agustin', 'wonderwardita', 'wards@gmail.com', 'wardita'),
(3, 'Mary Xylex Pasion', 'xyxylex', 'xylex@gmail.com', 'xylex'),
(5, 'Byun Baekhyun', 'bbh', 'bbh@gmail.com', 'baekhyun'),
(6, 'Park Chanyeol', 'pcy', 'pcy@gmail.com', 'chanyeol'),
(7, 'Kim Jun Myeon', 'kjm', 'kjm@gmail.com', 'junmyeon'),
(8, 'Kim Jongin', 'kji', 'kji@gmail.com', 'jongin'),
(9, 'Kim Jongdae', 'kjd', 'kjd@gmail.com', 'jongdae'),
(10, 'Oh Sehun', 'osh', 'osh@gmail.com', 'sehun'),
(11, 'Do Kyungsoo', 'dks', 'dks@gmail.com', 'kyungsoo'),
(17, 'Jezarene Valiente', 'jvaliente', 'jvaliente@gmail.com', 'jezarene'),
(18, 'Jezarene Valiente', 'jezarene', 'jvaliente@gmail.com', 'jezarene');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin_account`
--
ALTER TABLE `admin_account`
  ADD PRIMARY KEY (`admin_id`);

--
-- Indexes for table `exam`
--
ALTER TABLE `exam`
  ADD PRIMARY KEY (`exam_id`);

--
-- Indexes for table `login_status`
--
ALTER TABLE `login_status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_accounts`
--
ALTER TABLE `user_accounts`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin_account`
--
ALTER TABLE `admin_account`
  MODIFY `admin_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `exam`
--
ALTER TABLE `exam`
  MODIFY `exam_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=131;

--
-- AUTO_INCREMENT for table `login_status`
--
ALTER TABLE `login_status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;

--
-- AUTO_INCREMENT for table `user_accounts`
--
ALTER TABLE `user_accounts`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
