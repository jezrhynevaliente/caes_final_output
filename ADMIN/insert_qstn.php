<?php 
    session_start();
    include "perfect_function.php";

    $table_name = "exam";

    $type = $_POST['type'];
    $question = $_POST['question'];
    $answer = $_POST['answer'];
    $choice_one = $_POST['choice_one'];
    $choice_two = $_POST['choice_two'];
    $choice_three = ($_POST['choice_three']);

    $user_data = array(
        "type" => $type,
        "question" => $question,
        "answer" => $answer,
        "choice_one" => $choice_one,
        "choice_two" => $choice_two,
        "choice_three" => $choice_three,
        "quiz_status" => 0,
        "tama" => 0,
        "mali" => 0
    );
    
    echo insert($user_data, $table_name);
    $_SESSION["alert_msg"] = 1;
    header("Location: create_qstn.php");
?>