-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 18, 2020 at 04:48 AM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.3.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `caes`
--

-- --------------------------------------------------------

--
-- Table structure for table `user_accounts`
--

CREATE TABLE `user_accounts` (
  `user_id` int(11) NOT NULL,
  `fullname` varchar(65) NOT NULL,
  `username` varchar(65) NOT NULL,
  `email` varchar(65) NOT NULL,
  `password` varchar(65) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_accounts`
--

INSERT INTO `user_accounts` (`user_id`, `fullname`, `username`, `email`, `password`) VALUES
(1, 'Jezarene Valiente', 'jeza', 'jezarenevaliente@gmail.com', 'jeza'),
(2, 'Wardita Agustin', 'wonderwardita', 'wards@gmail.com', 'wardita'),
(3, 'Mary Xylex Pasion', 'xyxylex', 'xylex@gmail.com', 'xylex'),
(5, 'Byun Baekhyun', 'bbh', 'bbh@gmail.com', 'baekhyun'),
(6, 'Park Chanyeol', 'pcy', 'pcy@gmail.com', 'chanyeol'),
(7, 'Kim Jun Myeon', 'kjm', 'kjm@gmail.com', 'junmyeon'),
(8, 'Kim Jongin', 'kji', 'kji@gmail.com', 'jongin'),
(9, 'Kim Jongdae', 'kjd', 'kjd@gmail.com', 'jongdae'),
(10, 'Oh Sehun', 'osh', 'osh@gmail.com', 'sehun'),
(11, 'Do Kyungsoo', 'dks', 'dks@gmail.com', 'kyungsoo'),
(17, 'Jezarene Valiente', 'jvaliente', 'jvaliente@gmail.com', 'jezarene'),
(18, 'Jezarene Valiente', 'jezarene', 'jvaliente@gmail.com', 'jezarene');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `user_accounts`
--
ALTER TABLE `user_accounts`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `user_accounts`
--
ALTER TABLE `user_accounts`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
