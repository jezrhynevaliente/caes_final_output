-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 18, 2020 at 04:48 AM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.3.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `caes`
--

-- --------------------------------------------------------

--
-- Table structure for table `login_status`
--

CREATE TABLE `login_status` (
  `id` int(11) NOT NULL,
  `username` varchar(65) NOT NULL,
  `date` date NOT NULL,
  `hour` varchar(65) NOT NULL,
  `day` varchar(65) NOT NULL,
  `score` int(11) NOT NULL,
  `items` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `login_status`
--

INSERT INTO `login_status` (`id`, `username`, `date`, `hour`, `day`, `score`, `items`) VALUES
(3, 'jeza', '2020-03-16', '06:56:21pm', 'Monday', 3, 5),
(5, 'jeza', '2020-03-16', '07:00:50pm', 'Monday', 1, 5),
(6, 'jeza', '2020-03-16', '07:26:36pm', 'Monday', 2, 5),
(7, 'jeza', '2020-03-16', '07:28:10pm', 'Monday', 3, 5),
(8, 'jeza', '2020-03-16', '07:29:55pm', 'Monday', 4, 5),
(47, 'jvaliente', '2020-03-17', '10:59:18pm', 'Tuesday', 3, 5),
(49, 'jezarene', '2020-03-17', '11:04:21pm', 'Tuesday', 3, 5);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `login_status`
--
ALTER TABLE `login_status`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `login_status`
--
ALTER TABLE `login_status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
