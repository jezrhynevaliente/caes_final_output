<?php
    session_start();
    include "perfect_function.php";
?>

<html>
    <head>
        <link rel="stylesheet" href="style.css" type="text/css">
        <title>COMPUTER AIDED EXAMINATION SYSTEM</title>
    </head>
    <body>
    <center>
            <table class="table">
                <tr>
                <td rowspan="2" class="table_column_right">
                            <table>
                                <tr>
                                    <td class="title_column">
                                        <center><span class="title">COMPUTER AIDED</span></center>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <center><span class="title">EXAMINATION SYSTEM</span></center>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <img class="img_index" src="img/exam.png">
                                    </td>
                                </tr>
                            </table>
                    </td>
                    <td rowspan="2" class="table_column_left">
                        <center>
                        <table>
                            <form action="total_item.php" method="post">
                            <tr>
                                <td>
                                    <center><span class="items">ITEMS </span></center>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <center>
                                    <select name="total_items" class="question_select" required>
                                        <option value="">NUMBER OF ITEMS?</option>
                                        <option value="5">5 items</option>
                                        <option value="10">10 items</option>
                                        <option value="15">15 items</option>
                                        <option value="20">20 items</option>
                                    </select>
                                    </center>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <center><button class="question_button">BEGIN</button></center>
                                </td>
                            </tr>
                            </form>
                            <tr>
                                <td>
                                    <form action="index.php">
                                        <center><button class="question_button">EXIT</button></center>
                                    </form>
                                </td>
                            </tr>
                        </table>
                        </center>
                        
                    </td>
                </tr>
                <!--END OF FIRST ROW-->
            </table>
    </center>
    </body>
</html>