<?php 
    session_start();
    include "perfect_function.php";

    $table_name = "user_accounts";

    $fullname = $_POST['fullname'];
    $username = $_POST['username'];
    $email = $_POST['email'];
    $password = $_POST['password'];

    $user_data = array(
        "fullname" => $fullname,
        "username" => $username,
        "email" => $email,
        "password" => $password,
    );
    
    echo insert($user_data, $table_name);
    $_SESSION["alert_msg"] = 1;
    header("Location: register.php");
?>