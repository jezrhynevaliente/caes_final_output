<?php
session_start();

$playpage2 = $_GET['playpage2'];


if (!isset($_SESSION['username'])) {
    header("Location: index.php");
}
?>

<html>
    <head>
        <link rel="stylesheet" href="style.css" type="text/css">
        <title>COMPUTER AIDED EXAMINATION SYSTEM</title>
    </head>
    <body>
    <center>
            <table class="table">
                <tr>
                    <td rowspan="2" class="table_column_left">
                        <table>
                            <tr>
                                <td colspan="2">
                                     <span class="exit_now">
                                        <center>
                                        ARE YOU SURE YOU WANT TO STOP THE EXAM?
                                        </center>
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <form action="exit_proc.php">
                                        <center><button class="yes_no">YES</button></center>
                                    </form>
                                </td>
                                <td>
                                    <form action="playexit.php?playpage3=<?=$_POST['playpage3'];?>">
                                        <input type="hidden" value="<?=$playpage2?>" name="playpage3">
                                        <center><button class="yes_no no">NO </button></center>
                                    </form>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td rowspan="2" class="table_column_right">
                            <table>
                                <tr>
                                    <td class="title_column">
                                        <center><span class="title">COMPUTER AIDED</span></center>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <center><span class="title">EXAMINATION SYSTEM</span></center>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <img class="img_index" src="img/exam.png">
                                    </td>
                                </tr>
                            </table>
                    </td>
                </tr>
                <!--END OF FIRST ROW-->
            </table>
    </center>
    </body>
</html>