<?php
    include "perfect_function.php";
    include "final_score.php";
    include "score.php";
?>

<html>
    <head>
        <link rel="stylesheet" href="style.css" type="text/css">
        <title>COMPUTER AIDED EXAMINATION SYSTEM</title>
    </head>
    <body>
    <center>
            <table class="table">
                <tr>
                <td rowspan="2" class="table_column_left">
                            <table>
                                <tr>
                                    <td class="title_column">
                                        <center><span class="title">COMPUTER AIDED</span></center>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <center><span class="title">EXAMINATION SYSTEM</span></center>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <img class="img_index" src="img/exam.png">
                                    </td>
                                </tr>
                            </table>
                    </td>
                    <td rowspan="2" class="table_column_right">
                        <center>
                            <table>
                                <tr>
                                    <td colspan="2" class="congratulation">
                                        <center><span class="congrats">CONGRATULATIONS!</span></center>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="space" colspan="2">    
                                        <center><span class="score">YOU GOT AN SCORE OF <?=$row['score'];?>!</span></center>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <form action="play_again_proc.php">
                                            <center><button class="play_exit">PLAY AGAIN</button></center>
                                        </form>
                                    </td>
                                    <td>
                                        <form action="exit_now.php">
                                            <center><button class="play_exit exit">EXIT</button></center>
                                        </form>
                                    </td>
                                </tr>
                            </table>
                        </center>
                    </td>
                </tr>
                <!--END OF FIRST ROW-->
            </table>
    </center>
    </body>
</html>
