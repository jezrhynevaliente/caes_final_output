<?php
    session_start();
    include "perfect_function.php";
?>

<html>
    <head>
        <link rel="stylesheet" href="style.css" type="text/css">
        <title>COMPUTER AIDED EXAMINATION SYSTEM</title>
    </head>
    <body>
    <center>
            <table class="table">
                <tr>
                    <td rowspan="2" class="table_column_left">
                        <!--LOG-->
                        <form action="login_proc.php" method="post">
                            <center>
                            <table>
                                <tr>
                                    <td><span class="title_login"> LOG IN</span> </td>
                                </tr>
                                <tr>
                                    <td>
                                        <?php
                                            if (isset($_SESSION['alert_msg'])) {
                                                if ($_SESSION['alert_msg'] == 1) {
                                                    echo "
                                                    <div class=''>
                                                        <div style='color: red; font-weight: bold; text-align: center;'>
                                                        USERNAME OR PASSWORD IS INCORRECT. <br> PLEASE TRY AGAIN.
                                                        </div>
                                                    </div> ";
                                                    session_unset();
                                                }
                                            }
                                        ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <input type="text" name="username" class="index_input" placeholder=" Username" required autocomplete="off">
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <input type="password" name="password" class="index_input" placeholder=" Password" required autocomplete="off">
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <center>
                                            <button type="submit" class="index_button">
                                                LOG IN
                                            </button>
                                        </center>
                                    </td>
                                </tr>
                                <tr>
                                    <td ><center><span class="account">Don't have an account? </span><a href="register.php" class="signup"><b>Sign Up</b> </a></center></td>
                                </tr>
                            </table>
                            </center>
                        </form>
                    </td>
                    <td rowspan="2" class="table_column_right">
                        <table>
                            <tr>
                                <td class="title_column">
                                    <center><span class="title">COMPUTER AIDED</span></center>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <center><span class="title">EXAMINATION SYSTEM</span></center>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <img class="img_index" src="img/exam.png">
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <!--END OF FIRST ROW-->
            </table>
    </center>
    </body>
</html>