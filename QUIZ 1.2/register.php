<?php
    session_start();
    include "perfect_function.php";
?>

<html>
    <head>
        <link rel="stylesheet" href="style.css" type="text/css">
        <title>MY JOURNEY TO YOU</title>
    </head>
    <body>
        <center>
        <table class="table">
            <tr>
                <td rowspan="2" class="table_column_left">
                    <!--REGISTER-->
                    <form action="register_proc.php" method="post">
                    <center>
                        <table>
                            <tr>
                                <td><span class="title_register">REGISTER</span></td>
                            </tr>
                            <tr>
                                <td>
                                    <?php
                                    if (isset($_SESSION['alert_msg'])) {
                                            if ($_SESSION['alert_msg'] == 1) {
                                                echo "
                                                <div>
                                                    <div style='color: red; font-weight: bold; text-align: center;'>
                                                    YOU ARE SUCCESSFULLY REGISTERED.
                                                    </div>
                                                </div> ";
                                                session_unset();
                                            }
                                        }
                                    ?>
                                </td>
                            </tr>
                            <tr>
                                <td><input type="text" name="fullname" class="register_input" placeholder=" Fullname" required autocomplete="off"></td>
                            </tr>
                            <tr>
                                <td><input type="text" name="username" class="register_input" placeholder=" Username" required autocomplete="off"></td>
                            </tr>
                            <tr>
                                <td><input type="email" name="email" class="register_input" placeholder=" Email" required autocomplete="off"></td>
                            </tr>
                            <tr>
                                <td><input type="password" name="password" class="register_input" placeholder=" Password" required autocomplete="off"></td>
                            </tr>
                            
                            <tr>
                            <td>
                                <center>
                                    <button class="register_button">
                                        REGISTER
                                    </button>
                                </center>
                            </td>
                            </tr>
                            <tr>
                                <td>
                                    <center>
                                    <span class="account">Have an account? </span>
                                    <a href="index.php" class="signup"><b> Login</b></a>
                                    </center>
                                </td>
                            </tr>
                        </table>
                        </center>
                    </form>
                </td>
                <td rowspan="2" class="table_column_right">
                        <table>
                            <tr>
                                <td class="title_column">
                                    <center><span class="title">COMPUTER AIDED</span></center>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <center><span class="title">EXAMINATION SYSTEM</span></center>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <img class="img_index" src="img/exam.png">
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
        </table>
        </center>
    </body>
</html>



