<?php
session_start();

if (!isset($_SESSION['username'])) {
    header("Location: index.php");
}

include "perfect_function.php";
?>
<html>
    <head>
        <link rel="stylesheet" href="style.css" type="text/css">
    </head>
    <body style="background-color: #f1f3f4;">
    <center>
        <table class="table"
            <tr>
                <td colspan="2" class="table_column_left">
                    <center>
                    <table>
                        <tr><td class="space"></td></tr>
                        <tr>
                            <td class="question_column">
                                <form action="" method="post">
                                    <?php
                                    //QUESTION AND ANSWER
                                    $table_name = "exam";

                                    $mysql_query2 = "select * from exam where quiz_status = 0 and type = 5 and answer = 'False' limit 1";
                                    
                                    $user_data2 = custom_query($mysql_query2);
                                
                                    foreach($user_data2 as $key => $row) {
                                        $exam_id = $row['exam_id'];
                                        $question = $row['question'];
                                        $type = $row['type'];
                                        $answer = $row['answer'];   
                                        $choice_one = $row['choice_one'];
                                        $choice_two= $row['choice_two']; 
                                        $choice_three = $row['choice_three'];
                                        $check = $row['tama']; 
                                        $wrong = $row['mali'];

                                    }
                                    ?>
                                    <center><span class="question"><?=$question;?></span></center>
                                </form>
                            </td>
                        </tr>
                        <tr><td class="space"></td></tr>
                        <!--UPDATE THE RECORD IF HE/SHE ANSWERED THE QUESTION RIGHT OR WRONG.-->
                            <?php include "check_wrong.php"; ?>
                            
                            <?php
                                if (isset($_POST['check'])){?> 
                                    <tr>
                                        <td colspan="2">
                                        <center>
                                            <img src="img/check.png" class="images"><br>
                                            <span class="ans_wro">YOUR ANSWER IS CORRECT!</span>
                                        </center>
                                        </td>
                                    </tr>   
                            <?php
                                } elseif (isset($_POST['wrong_one'])){?>
                                    <tr>
                                        <td colspan="2">
                                        <center>
                                            <img src="img/wrong.png" class="images"> <br>
                                            <span class="ans_wro">YOUR ANSWER IS WRONG!</span>
                                        </center>
                                        </td>
                                    </tr>
                            <?php } ?>
                    </table>
                    </center>
                </td>
            <form action="playsix.php" method="post">
                <td class="table_column_right">
                    <!--LETTER A-->
                    <center>
                    <?php if (isset($_POST['check'])){?>
                    <table>
                        <tr>
                            <td><input class="button" type="button" name="wrong_one" value="A. <?= $choice_one ?>"></td>
                        </tr>
                        <tr>
                            <td><input class="button" type="button" style="background-color: rgb(167, 255, 167);" name="check" value="B. <?= $answer ?>"></td>
                        </tr>
                        
                    </table>
                    <?php } elseif (isset($_POST['wrong_one'])){?>
                    <table>
                        <tr>
                            <td><input class="button" type="button" style="background-color: rgb(255, 126, 126);"  name="wrong_one" value="A. <?= $choice_one ?>"></td>
                        </tr>
                        <tr>
                            <td><input class="button" type="button" name="check" value="B. <?= $answer ?>"></td>
                        </tr>
                         
                    </table>
                    <?php } else {?>
                    <table>
                        <tr>
                            <td><button class="button" name="wrong_one" >A. <?= $choice_one ?></button></td>
                        </tr>
                        <tr>
                            <td><button class="button" name="check" >B. <?= $answer ?></button></td>
                        </tr>
                    </table>
                    <?php } ?>
                    </center>
            </form> 
                <!--COUNT THE SCORE HE/SHE GARNERED-->
                    <?php include "score.php";
                            include "total.php";
                            include "answered.php";
                            
                    if ($items - 1 == $answered) { ?>
                    
                    <table class="next_exit2">
                        <tr>
                            <td>
                                <form action="exit.php?playpage2=<?=$_POST['playpage2'];?>">
                                <input type="hidden" value="playsix.php" name="playpage2">
                                    <center><button class="next"> EXIT </button></center>
                                </form>
                            </td>
                            <td>
                                <form action="finished_all_proc.php?exam_id=<?=$_POST['exam_id'];?>">
                                    <input type="hidden" name="exam_id" value="<?=$exam_id?>">
                                    <center><button class="next"> NEXT </button></center>
                            </form>
                            </td>
                        </tr>
                    </table>
                    <?php } else { ?>
                    <table class="next_exit2">
                    <tr>
                        <td>
                            <form action="exit.php?playpage2=<?=$_POST['playpage2'];?>">
                            <input type="hidden" value="playsix.php" name="playpage2">
                                <center><button class="next"> EXIT </button></center>
                            </form>
                        </td>
                        <td>
                            <form action="play_proc.php?exam_id=<?=$_POST['exam_id'];?>">
                                <input type="hidden" name="exam_id" value="<?=$exam_id?>">
                                <center><button class="next"> NEXT </button></center>
                            </form>
                        </td>
                    </tr>   
                    </table>
                    <?php } ?>
                </td>
            </tr>
        </table>
        </center>
    </body>  
</html>


